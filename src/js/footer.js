class MyFooter extends HTMLElement {
    connectedCallback(){
        this.innerHTML = 
    ` 
    <footer>
    <div class="container-fluid bg-secondary pt-md-5 pt-2" style="height:9rem">
    <div class="row">
            <div class="col-12 col-md-2 col-lg-3 col-xl-4 fs-2 ms-0 pe-5 pe-md-0 ps-0 ms-md-5 text-center text-md-start mb-2">
            <a href="index.html" class="logo link-dark" style="text-decoration: none;"><img src="build/images/Logo.png" style="height:4rem" class="img-fluid footlogo" alt=""></a>
            </div>
            <div class="col-12 col-md-9 col-lg-8 col-xl-7 d-flex ms-0">
                <div class="col d-flex ms-5 ms-md-0">
                    <div class="col-auto d-none d-md-block bg-dark rounded-2 fs-1 me-5">|</div>
                    <div class="col fs-2 col-auto pe-2 pe-sm-1 pe-md-0"> <i class="fas fa-map-marker"></i> </div>
                    <div class="col fs-5 ms-1 ms-sm-4 pe-2">
                        <div class="row col-auto fw-bold">Pensiunea Agape</div>
                        <div class="row fw-light">Salaj, Zalau</div>
                    </div>
                </div>
                <div class="col d-flex ms-5 ms-md-0">
                    <div class="col fs-2 col-auto pe-2 pe-sm-1 pe-md-0"> <i class="fas fa-comments"></i> </div>
                    <div class="col fs-5 ms-1 ms-sm-4">
                        <div class="row fw-bold">Disponibil 24/7</div>
                        <div class="row fw-light">+40-745-597921</div>
                    </div>
                </div>
            </div>
    </div>
</div>
</footer>
        `
    }
}

customElements.define('my-footer', MyFooter)