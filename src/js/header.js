class MyHeader extends HTMLElement {
    connectedCallback(){
        this.innerHTML = 
    ` <nav class="navbar fixed-top border-bottom border-dark navbar-light">
    <div class="container-fluid">
        <div class="d-flex w-100">
            <a class="navbar-brand logo ms-2 ms-sm-5" href="index.html">
                <div class="d-flex">
                    <img src="build/images/Logo.png" style="height:5rem" class="img-fluid logo1" alt="">
                    <div class="d-flex fst-italic logo2 ms-1 align-items-center" style="font-size:2rem">Pensiunea Agape</div>
                </div>
            </a>
            <div class="d-flex justify-content-end align-items-center w-100">
                <input class="form-control me-2 me-sm-4 h-50 w-50" type="search" placeholder="Search" aria-label="Search">
                <button class="navbar-toggler me-2 me-sm-4" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </div>
        <div class="collapse navbar-collapse mb-3" id="navbarSupportedContent">
            <div class="row row-cols-2 row-cols-md-3 row-cols-xxl-5 pt-5 ms-4">
                <div class="col d-flex flex-column align-items-center fs-2">  
                <a href="camera-rezervari.html" class="link-dark" style="text-decoration: none;"><i class="fas raise fa-bed"></i></a>
                <a href="camera-rezervari.html" class="link link--dia link-dark myfont" style="text-decoration: none;"> Camere&Rezervari </a></div>

                <div class="col d-flex flex-column align-items-center fs-2">  
                <a href="piscina-sauna.html" class="link-dark" style="text-decoration: none;"><i class="fas raise fa-swimming-pool"></i></a>
                <a href="piscina-sauna.html" class="link link--dia link-dark myfont" style="text-decoration: none;"> Welness&Spa </a></div>

                <div class="col d-flex flex-column align-items-center fs-2">  
                <a href="food-events.html" class="link-dark" style="text-decoration: none;"><i class="fas raise fa-utensils"></i></a>
                <a href="food-events.html" class="link link--dia link-dark myfont" style="text-decoration: none;"> Food&Events </a></div>

                <div class="col d-flex col-md-6 col-lg-6 col-xl-6 flex-column align-items-center fs-2">   
                <a href="locuri-de-vizitat.html" class="link-dark" style="text-decoration: none;"><i class="fas raise fa-map-marked-alt"></i></a>
                <a href="locuri-de-vizitat.html" class="link link--dia link-dark myfont" style="text-decoration: none;"> Locuri de vizitat </a></div>

                <div class="col d-flex col-md-6 col-lg-6 col-xl-6 col-12 flex-column align-items-center fs-2">  
                <a href="contact.html" class="link-dark" style="text-decoration: none;"> <i class="fas raise fa-lightbulb"></i></a>
                <a href="contact.html" class="link link--dia link-dark myfont" style="text-decoration: none;"> Contact </a></div>
            </div>
        </div>
    </div>
    </nav>
        `
    }
}

customElements.define('my-header', MyHeader)