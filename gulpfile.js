var gulp = require("gulp");
var concat = require("gulp-concat");
var minify = require("gulp-minify");
var sass = require("gulp-sass")(require("sass"));
var imagemin = require("gulp-imagemin");
var cleanCss = require("gulp-clean-css");
var del = require("del");

gulp.task("pack-js", function () {
  return gulp
    .src([
        "aos/aos.js",
        "node_modules/@fortawesome/fontawesome-free/js/all.js",
        "node_modules/bootstrap/dist/js/bootstrap.min.js",
        "src/js/*.js"
])
    .pipe(concat("pensiune.js"))
    .pipe(minify())
    .pipe(gulp.dest("public/build/js"));
});

gulp.task("pack-css", function () {
  return gulp
    .src([
        "aos/aos.css",
        "node_modules/@fortawesome/fontawesome-free/css/all.css",
        "node_modules/bootstrap/dist/css/bootstrap.min.css",
        "assets/css/*.css",
    ])
    .pipe(concat("pensiune.css"))
    .pipe(cleanCss())
    .pipe(gulp.dest("public/build/css"));
});

gulp.task("scss", function () {
  return gulp
    .src("src/scss/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest("assets/css"));
});

gulp.task("pack-img", function () {
  return gulp.src("src/images/*.*")
    .pipe(imagemin())
    .pipe(gulp.dest("public/build/images"));
});

gulp.task("clean-js", function () {
  return del(["public/build/js/*.js"]);
});

gulp.task("clean-css", function () {
  return del(["public/build/css/*.css"]);
});

gulp.task("clean-images", function () {
  return del(["public/build/images/*"]);
});

gulp.task("watch", function () {
  gulp.watch("src/js/*.js", gulp.series("clean-js", "pack-js"));
  gulp.watch(
    "src/scss/*.scss",
    gulp.series("clean-css", "scss", "pack-css")
  );
  gulp.watch("src/images/*.*", gulp.series("clean-images", "pack-img"));
});

gulp.task("default", gulp.series("watch"));
